const Book = require('../models/Book')
const Author = require('../models/Author')

const mongoDataMethods = {
	getAllBooks: async (condition = null) =>
		condition === null ? await Book.find() : await Book.find(condition),
	getBookById: async id => await Book.findById(id),
	getAllAuthors: async () => await Author.find(),
	getAuthorById: async id => await Author.findById(id),
	createAuthor: async args => {
		const newAuthor = new Author(args)
		return await newAuthor.save()
	},
	createBook: async args => {
		const newBook = new Book(args)
		return await newBook.save()
	},
	updateAuthor: async args => {
		let id = args.id
		let name = args.name
		let age = args.age
		
		return await Author.findOneAndUpdate(
			{
				_id: id,
			},
			{
			$set: {
				name,
				age
			}
			},
			{
			new: true
			}
		);
		
	},
	deleteAuthor: async  args => {
		await Author.findOneAndRemove({ _id: args.id});
			 
	},
	
}

module.exports = mongoDataMethods
