const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type Result {
    status: Int!
    message: String
  }

  type Book {
    id: ID
    name: String
    genre: String
    author: Author
  }

  type Author {
    id: ID!
    name: String
    age: Int
    books: [Book]
  }

  type Query {
    books: [Book]
    book(id: ID!): Book
    authors: [Author]
    author(id: ID!): Author
  }

  type Mutation {
    createAuthor(name: String, age: Int): Author
    createBook(name: String, genre: String, authorId: ID!): Book
    updateAuthor(id: ID!, name: String, age: Int): Result!
    deleteAuthor(id: ID!): Result!
  }
`;

module.exports = typeDefs;
